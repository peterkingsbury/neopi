'use strict';

class Sensor {
  constructor(options) {
    if (!options) {
      throw new Error('invalid options');
    }

    this._db = options.db;
  }

  /**
   * Store a set of readings for this sensor, given the timestamp.
   * @param properties The properties to be stored.
   * @param timestamp The effective timestamp for all properties.
   * @returns {*}
   */
  store(properties, timestamp) {
    return this._db.transaction(t => {
      return this._db.models.sensor.findOrCreate({
        where: {
          name: this.constructor.name
        },
        defaults: {
          name: this.constructor.name
        },
        transaction: t
      }).spread((result, created) => {
        let sensorType = result.get({ plain: true });
        return this._db.models.reading.create({
          timestamp: timestamp,
          sensor_id: sensorType.id
        }, { transaction: t }).then(result => {
          let reading = result.get({ plain: true });
          console.log(result.get({ plain: true }));
          const keys = Object.keys(properties);
          let ops = [];
          for (const key of keys) {
            let value = properties[key];
            ops.push(this._db.models.property.findOrCreate({
              where: {
                name: key
              },
              defaults: {
                name: key
              },
              transaction: t
            }).spread((result, created) => {
              let property = result.get({ plain: true });
              return this._db.models.reading_property.create({
                reading_id: reading.id,
                property_id: property.id,
                value: value
              }, { transaction: t });
            }));
          }
          return Promise.all(ops);
        });
      });
    });
  }
}

module.exports.Sensor = Sensor;
