'use strict';

const Sensor = require('../core/Sensor').Sensor;

const Buffer = require('buffer').Buffer;
const i2c = require('i2c-bus');

/*
const MPL3115A2           = 0x60;
const cmdControlRegister1 = 0x26;
const cmdWhoAmI           = 0x0C;
const cmdDataConfig       = 0x13;
*/
const sleepMs             = 1000;

function sleep(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) { /!* Do nothing *!/ }
}

class MPL3115A2 extends Sensor {
  constructor(options) {
    super(options);
    this.bus = null;
    this.address = 0x60;
    this.pressure = 0;
    this.temperature = 0;
    this.altitude = 0;
  }

  read() {
    return this.update().then(() => {
      return {
        pressure: this.pressure,
        temperature: this.temperature,
        altitude: this.altitude
      };
    });
  }

  updateSync() {
    this.bus = i2c.openSync(1);

    // MPL3115A2 address, 0x60(96)
    // Select control register, 0x26(38)
    // 0xB9(185) Active mode, OSR = 128, Altimeter mode
    this.bus.writeByteSync(0x60, 0x26, 0xB9);

    // MPL3115A2 address, 0x60(96)
    // Select data configuration register, 0x13(19)
    // 0x07(07) Data ready event enabled for altitude, pressure, temperature
    this.bus.writeByteSync(0x60, 0x13, 0x07);

    // MPL3115A2 address, 0x60(96)
    // Select control register, 0x26(38)
    // 0xB9(185) Active mode, OSR = 128, Altimeter mode
    this.bus.writeByteSync(0x60, 0x26, 0xB9);

    sleep(sleepMs);

    // MPL3115A2 address, 0x60(96)
    // Read data back from 0x00(00), 6 bytes
    // status, tHeight MSB1, tHeight MSB, tHeight LSB, temp MSB, temp LSB
    let data = Buffer.alloc(6);
    let tBufBytesRead = this.bus.readI2cBlockSync(0x60, 0x00, 6, data);

    // Convert the data to 20-bits
    let tHeight = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16;
    let temp = ((data[4] * 256) + (data[5] & 0xF0)) / 16;
    let altitude = tHeight / 16.0;
    let cTemp = temp / 16.0;
    let fTemp = cTemp * 1.8 + 32;

    // MPL3115A2 address, 0x60(96)
    // Select control register, 0x26(38)
    // 0x39(57) Active mode, OSR = 128, Barometer mode
    this.bus.writeByteSync(0x60, 0x26, 0x39);

    sleep(sleepMs);

    // MPL3115A2 address, 0x60(96)
    // Read data back from 0x00(00), 4 bytes
    // status, pres MSB1, pres MSB, pres LSB
    data = Buffer.alloc(4);
    let pBufBytesRead = this.bus.readI2cBlockSync(0x60, 0x00, 4, data);

    // Convert the data to 20-bits
    let pres = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16;
    let pressure = (pres / 4.0) / 1000.0;

    // Output data to screen
    //    console.log(`Pressure:    ${pressure} kPa`);
    //    console.log(`Altitude:    ${altitude} m`);
    //    console.log(`Temperature: ${cTemp} C (${fTemp} F)`);

    this.bus.closeSync();

    this.temperature = cTemp;
    this.altitude = altitude;
    this.pressure = pressure;
  }

  update() {

    return new Promise((resolve, reject) => {
      this.bus = i2c.open(1, err => {
        if (err) {
          return reject(new Error('unable to open i2c bus'));
        }

        // MPL3115A2 address, 0x60(96)
        // Select control register, 0x26(38)
        // 0xB9(185) Active mode, OSR = 128, Altimeter mode
        this.bus.writeByte(0x60, 0x26, 0xB9, err => {
          if (err) {
            return reject(new Error('failed to select control register'));
          }

          // MPL3115A2 address, 0x60(96)
          // Select data configuration register, 0x13(19)
          // 0x07(07) Data ready event enabled for altitude, pressure, temperature
          this.bus.writeByte(0x60, 0x13, 0x07, err => {
            if (err) {
              return reject(new Error('unable to select data configuration register'));
            }

            // MPL3115A2 address, 0x60(96)
            // Select control register, 0x26(38)
            // 0xB9(185) Active mode, OSR = 128, Altimeter mode
            this.bus.writeByte(0x60, 0x26, 0xB9, err =>{
              if (err) {
                return reject(new Error('unable to select control register'));
              }

              //sleep(sleepMs);

              setTimeout(() => {

                // MPL3115A2 address, 0x60(96)
                // Read data back from 0x00(00), 6 bytes
                // status, tHeight MSB1, tHeight MSB, tHeight LSB, temp MSB, temp LSB
                let data = Buffer.alloc(6);
                this.bus.readI2cBlock(0x60, 0x00, 6, data, (err, bytesRead, buffer) => {
                  if (err) {
                    return reject(new Error('unable to read temperature and altitude data'));
                  }

                  // Convert the data to 20-bits
                  let tHeight = ((buffer[1] * 65536) + (buffer[2] * 256) + (buffer[3] & 0xF0)) / 16;
                  let temp = ((buffer[4] * 256) + (buffer[5] & 0xF0)) / 16;
                  let altitude = tHeight / 16.0;
                  let cTemp = temp / 16.0;
                  let fTemp = cTemp * 1.8 + 32;

                  // MPL3115A2 address, 0x60(96)
                  // Select control register, 0x26(38)
                  // 0x39(57) Active mode, OSR = 128, Barometer mode
                  this.bus.writeByte(0x60, 0x26, 0x39, err => {
                    if (err) {
                      return reject(new Error('unable to read data'));
                    }

                    setTimeout(() => {
                      // MPL3115A2 address, 0x60(96)
                      // Read data back from 0x00(00), 4 bytes
                      // status, pres MSB1, pres MSB, pres LSB
                      data = Buffer.alloc(4);
                      this.bus.readI2cBlock(0x60, 0x00, 4, data, (err, bytesRead, buffer) => {
                        if (err) {
                          return reject(new Error('unable to read pressure data'));
                        }

                        // Convert the data to 20-bits
                        let pres = ((buffer[1] * 65536) + (buffer[2] * 256) + (buffer[3] & 0xF0)) / 16;
                        let pressure = (pres / 4.0) / 1000.0;

                        this.bus.close(err => {
                          if (err) {
                            return reject(new Error('unable to close i2c bus'));
                          }

                          this.temperature = cTemp;
                          this.altitude = altitude;
                          this.pressure = pressure;

                          resolve();
                        });
                      });
                    }, sleepMs);
                  });
                });
              }, sleepMs);
            });
          });
        });
      });
    });
  }
}

module.exports.MPL3115A2 = MPL3115A2;
