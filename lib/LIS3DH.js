'use strict';

const Sensor = require('../core/Sensor').Sensor;

const Buffer = require('buffer').Buffer;
const i2c = require('i2c-bus');

const CTRL_REG_1 = 0x20;
const OUT_X_L    = 0x28;
const OUT_X_H    = 0x29;
const OUT_Y_L    = 0x2a;
const OUT_Y_H    = 0x2b;
const OUT_Z_L    = 0x2c;
const OUT_Z_H    = 0x2d;

class LIS3DH extends Sensor {
  constructor(options) {
    super(options);
    this.bus = null;
    this.address = 0x18;
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }

  read() {
    return this.update().then(() => {
      return {
        x: this.x,
        y: this.y,
        z: this.z
      };
    });
  }

  updateSync() {
    this.bus = i2c.openSync(1);

    // Enable sensor
    this.bus.writeByteSync(this.address, CTRL_REG_1, 0x7f);

    let xl = this.bus.readByteSync(this.address, OUT_X_L);
    let xh = this.bus.readByteSync(this.address, OUT_X_H);
    let yl = this.bus.readByteSync(this.address, OUT_Y_L);
    let yh = this.bus.readByteSync(this.address, OUT_Y_H);
    let zl = this.bus.readByteSync(this.address, OUT_Z_L);
    let zh = this.bus.readByteSync(this.address, OUT_Z_H);

    this.x = ( (xh << 8) | xl ) >> 4;
    this.y = ( (yh << 8) | yl ) >> 4;
    this.z = ( (zh << 8) | zl ) >> 4;
    this.x /= 1024;
    this.y /= 1024;
    this.z /= 1024;

    this.bus.closeSync();
  }

  update() {
    return new Promise((resolve, reject) => {
      this.bus = i2c.open(1, err => {
        if (err) {
          return reject(new Error('unable to open i2c bus'));
        }

        // Enable sensor
        this.bus.writeByte(this.address, CTRL_REG_1, 0x7f, err => {
          if (err) {
            return reject(new Error('unable to enable sensor'));
          }

          //let buffer = new Buffer(6);
          //this.bus.readI2cBlock(this.address, 0x28, 6, buffer, (err, bytesRead, buffer) => {
          //
          //  if (err) {
          //    return reject(new Error('unable to read accelerometer data'));
          //  }
          //
          //  console.log(buffer);
          //
          //  this.x = ( (buffer[1] << 8) | buffer[0] ) >> 4;
          //  this.y = ( (buffer[3] << 8) | buffer[2] ) >> 4;
          //  this.z = ( (buffer[5] << 8) | buffer[4] ) >> 4;
          //
          //  this.x /= 1024;
          //  this.y /= 1024;
          //  this.z /= 1024;
          //
          //  this.bus.close(err => {
          //    if (err) {
          //      return reject(new Error('unable to close i2c bus'));
          //    }
          //
          //    resolve();
          //  });
          //});

          let xl = this.bus.readByteSync(this.address, OUT_X_L);
          let xh = this.bus.readByteSync(this.address, OUT_X_H);
          let yl = this.bus.readByteSync(this.address, OUT_Y_L);
          let yh = this.bus.readByteSync(this.address, OUT_Y_H);
          let zl = this.bus.readByteSync(this.address, OUT_Z_L);
          let zh = this.bus.readByteSync(this.address, OUT_Z_H);

          this.x = ( (xh << 8) | xl ) >> 4;
          this.y = ( (yh << 8) | yl ) >> 4;
          this.z = ( (zh << 8) | zl ) >> 4;
          this.x /= 1024;
          this.y /= 1024;
          this.z /= 1024;

          this.bus.close(err => {
            if (err) {
              return reject(new Error('unable to close i2c bus'));
            }

            resolve();
          });


        });
      });
    });
  }

}

module.exports.LIS3DH = LIS3DH;
