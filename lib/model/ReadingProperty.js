'use strict';

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reading_property', {
    value: {
      type: DataTypes.STRING,
      field: 'value'
    },
    reading_id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'reading',
        key: 'id'
      }
    },
    property_id: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      references: {
        model: 'property',
        key: 'id'
      }
    },
  }, {
    freezeTableName: true,
    timestamps: false
  });
};
