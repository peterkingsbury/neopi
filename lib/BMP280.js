'use strict';

const Sensor = require('../core/Sensor').Sensor;

class BMP280 extends Sensor {
  constructor(options) {
    super(options);
    this.bus = null;
    this.address = 0x77;
    this.pressure = 0;
    this.temperature = 0;
    this.altitude = 0;

  }
}

module.exports.BMP280 = BMP280;
