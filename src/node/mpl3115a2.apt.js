'use strict';

const Buffer = require('buffer').Buffer;
const i2c = require('i2c-bus');

const MPL3115A2           = 0x60;
const cmdControlRegister1 = 0x26;
const cmdWhoAmI           = 0x0C;
const cmdDataConfig       = 0x13;
const sleepMs             = 1000;

function sleep(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) { /!* Do nothing *!/ }
}

let bus = i2c.openSync(1);

// MPL3115A2 address, 0x60(96)
// Select control register, 0x26(38)
// 0xB9(185) Active mode, OSR = 128, Altimeter mode
bus.writeByteSync(0x60, 0x26, 0xB9);

// MPL3115A2 address, 0x60(96)
// Select data configuration register, 0x13(19)
// 0x07(07) Data ready event enabled for altitude, pressure, temperature
bus.writeByteSync(0x60, 0x13, 0x07);

// MPL3115A2 address, 0x60(96)
// Select control register, 0x26(38)
// 0xB9(185) Active mode, OSR = 128, Altimeter mode
bus.writeByteSync(0x60, 0x26, 0xB9);

sleep(sleepMs);

// MPL3115A2 address, 0x60(96)
// Read data back from 0x00(00), 6 bytes
// status, tHeight MSB1, tHeight MSB, tHeight LSB, temp MSB, temp LSB
let data = Buffer.alloc(6);
let tBufBytesRead = bus.readI2cBlockSync(0x60, 0x00, 6, data);

// Convert the data to 20-bits
let tHeight = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16;
let temp = ((data[4] * 256) + (data[5] & 0xF0)) / 16;
let altitude = tHeight / 16.0;
let cTemp = temp / 16.0;
let fTemp = cTemp * 1.8 + 32;

// MPL3115A2 address, 0x60(96)
// Select control register, 0x26(38)
// 0x39(57) Active mode, OSR = 128, Barometer mode
bus.writeByteSync(0x60, 0x26, 0x39);

sleep(sleepMs);

// MPL3115A2 address, 0x60(96)
// Read data back from 0x00(00), 4 bytes
// status, pres MSB1, pres MSB, pres LSB
data = Buffer.alloc(4);
let pBufBytesRead = bus.readI2cBlockSync(0x60, 0x00, 4, data);

// Convert the data to 20-bits
let pres = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16;
let pressure = (pres / 4.0) / 1000.0;

// Output data to screen
console.log(`Pressure:    ${pressure} kPa`);
console.log(`Altitude:    ${altitude} m`);
console.log(`Temperature: ${cTemp} C (${fTemp} F)`);

bus.closeSync();
