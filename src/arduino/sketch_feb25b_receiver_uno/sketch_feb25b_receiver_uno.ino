#include <SPI.h>
#include <RH_RF95.h>

#define RFM95_CS 10
#define RFM95_RST 9
#define RFM95_INT 2

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0

// Singleton instance of the radio driver
RH_RF95 rf95(RFM95_CS, RFM95_INT);

#define LED_RX 5
#define LED_TX 6
#define BLINK_DELAY 10

void setup() 
{
  pinMode(LED_RX, OUTPUT);
  pinMode(LED_TX, OUTPUT);
  
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  while (!Serial);
  Serial.begin(115200);
  delay(100);

  Serial.println("RFM9x Receiver");
  
  // manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("Failed to initialize LoRA radio unit");
    while (1);
  }
  Serial.println("Initialized LoRA radio unit");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Frequency set to ");
  Serial.print(RF95_FREQ);
  Serial.println(" MHz");

  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);
}

void loop()
{
  if (rf95.available()) {
    // Should be a message for us now   
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    memset(buf, 0, len);

    digitalWrite(LED_RX, HIGH);
    delay(BLINK_DELAY);
    bool received = rf95.recv(buf, &len);
    digitalWrite(LED_RX, LOW);
    
    if (received) {
      //RH_RF95::printBuffer("Received: ", buf, len);
      //Serial.print("Got: ");
      Serial.print("RECV: ");
      Serial.print((char*)buf);
      Serial.print(" (RSSI=");
      Serial.print(rf95.lastRssi(), DEC);
      Serial.println(")");
      
      // Send a reply
      uint8_t data[] = "OK";
      digitalWrite(LED_TX, HIGH);
      delay(BLINK_DELAY);
      rf95.send(data, sizeof(data));
      rf95.waitPacketSent();
      digitalWrite(LED_TX, LOW);

      //Serial.println("Sent a reply");
    } else {
      Serial.println("RECV failed");
    }
  }
}

